package com.example.hp.androidfacebooklogin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

public class MainActivity extends AppCompatActivity {

    private CallbackManager callbackManager;
    private TextView txtEmail,txtBirthday,txtFriends,txtFirstname,txtLastname,txtGender;
    private ImageView imgAvater;
    private ProgressDialog mDialog;

    /**
     * ctrl+o to find onActivityResult method
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        /**
         * It is necessary to find Key Hash to use fb login when we start to make a project
         */
        //printKeyHash();

        callbackManager=CallbackManager.Factory.create();

        txtBirthday=findViewById(R.id.birthday);
        txtEmail=findViewById(R.id.email);
        txtFriends=findViewById(R.id.friends);
        txtFirstname=findViewById(R.id.first_name);
        txtLastname=findViewById(R.id.last_name);
        txtGender=findViewById(R.id.gender);

        imgAvater=findViewById(R.id.avater);

        LoginButton loginButton=findViewById(R.id.login_button);

        loginButton.setReadPermissions(Arrays.asList("public_profile","email","user_birthday","user_friends"));

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                mDialog=new ProgressDialog(MainActivity.this);
                mDialog.setMessage("Retrieving Data.....");
                mDialog.show();

                String accessToken=loginResult.getAccessToken().getToken();

                GraphRequest request=GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        mDialog.dismiss();
                        Log.d( "response",response.toString());
                        getData(object);
                    }
                });

                /**
                 * Request graph API
                 */
                Bundle parameters=new Bundle();
                parameters.putString("fields","id,first_name,last_name,email,gender,birthday,friends");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        /**
         * if already logged in
         */
        if(AccessToken.getCurrentAccessToken()!=null){
            txtEmail.setText(AccessToken.getCurrentAccessToken().getUserId());
        }

    }

    private void getData(JSONObject object) {

        try{
            /**
             * getting profile picture but gives a problem..
             */
            URL profile_picture=new URL("https://graph.facebook.com/"+object.getString("id")+"/picture?width=250&height=250");
            Picasso.with(this).load(profile_picture.toString()).into(imgAvater);

            //getting first_name
            txtFirstname.setText("First name: "+object.getString("first_name"));

            //getting last name
            txtLastname.setText("Last name: "+object.getString("last_name"));


            //getting email
            txtEmail.setText("Email: "+object.getString("email"));



            //getting male or female
            txtGender.setText("Gender: "+object.getString("gender"));

            //birthdate
            txtBirthday.setText("BirthDate: "+object.getString("birthday"));

            //friends number
            txtFriends.setText("Friends: "+object.getJSONObject("friends").getJSONObject("summary").getString("total_count"));

            //Log.d("MainActivity", "json: "+object);

        } catch (MalformedURLException e) {
            ;
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * use it only for getting KeyHash
     */
    private void printKeyHash() {

        try {
            PackageInfo info=getPackageManager().getPackageInfo("com.example.hp.androidfacebooklogin", PackageManager.GET_SIGNATURES);
            for(Signature signature:info.signatures){
                MessageDigest md=MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash: ", android.util.Base64.encodeToString(md.digest(), android.util.Base64.DEFAULT));
            }

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}

